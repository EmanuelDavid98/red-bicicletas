var Bicicleta=require('../../models/bicicleta');
var request=require('request');
var server=require('../../bin/www');

describe('Bicicleta API', ()=>{
    describe('GET BICICLCETAS /', () => {
        it('status 200',()=>{
            expect(Bicicleta.allBicis.length).toBe(0);
            var a = new Bicicleta(1,'rojo','urbana',[10.9838039,-74.8882299]);
            Bicicleta.add(a);
            request.get('http://localhost:3000/api/bicicletas', function(error,response,body){
                expect(response.statusCode).toBe(200)
            })
        })

    })
    describe('POST BICICLCETAS /create', () => {
        it('status 200',(done)=>{
            var headers={'content-type':'application/json'};
            var aBici='{"id":10,"color":"rojo","modelo":"urbana","lat":-32,"lng":-34}';
            request.post({
                headers:headers,
                url:'http://localhost:3000/api/bicicletas/create',
                body:aBici
            },function(error,response,body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe("rojo");
                expect(Bicicleta.allBicis.length).toBe(1);
                done();
            })
        })

    })

    describe('DELETE BICICLCETAS /delete', () => {
        it('status 200',(done)=>{
            var headers={'content-type':'application/json'};
            var aBici='{"id":10,"color":"rojo","modelo":"urbana","lat":-32,"lng":-34}';
            request.post({
                headers:headers,
                url:'http://localhost:3000/api/bicicletas/create',
                body:aBici
            },function(error,response,body){
                expect(Bicicleta.allBicis.length).toBe(1);
                Bicicleta.removeById(10);
                expect(Bicicleta.allBicis.length).toBe(0);
                done();
            })
        })
    })

    describe('UPDATE BICICLCETAS /update', () => {
        it('status 200',(done)=>{
            var headers={'content-type':'application/json'};
            var aBici='{"id":10,"color":"rojo","modelo":"urbana","lat":-32,"lng":-34}';
            request.post({
                headers:headers,
                url:'http://localhost:3000/api/bicicletas/create',
                body:aBici
            },function(error,response,body){
              Bicicleta.findById(10).modelo="montana";
              Bicicleta.findById(10).color="verde";
              expect(Bicicleta.findById(10).color).toBe("verde");
              expect(Bicicleta.findById(10).modelo).toBe("montana");

              done();
            })
            
        })
    })
})